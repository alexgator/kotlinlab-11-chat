package com.cornel.lab11chat.common

import java.net.InetAddress

data class User (var name: String = "Anonymous user", val ip: InetAddress, val port: Int)