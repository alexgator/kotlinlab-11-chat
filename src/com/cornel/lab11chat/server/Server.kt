package com.cornel.lab11chat.server

import com.cornel.lab11chat.common.Keyword
import com.cornel.lab11chat.common.User
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress


class Server(port: Int, private val logging: Boolean = false) : Thread() {
    companion object {
        private val MAX_PACKAGE_SIZE    = 65500
    }

    private val socket      = DatagramSocket(port)
    private var connected   = emptyList<User>()

    override fun run() {
        while (true) {
            val receiveData     = ByteArray(MAX_PACKAGE_SIZE)
            val receivePacket   = DatagramPacket(receiveData, receiveData.size)
            socket.receive(receivePacket)

            if (logging) logInfo("Incoming package from ${receivePacket.socketAddress}")

            val userIP      = receivePacket.address
            val userPort    = receivePacket.port
            val message     = String(receivePacket.data).substring(0, receivePacket.length)
            val user        = findOrAddUser(userIP, userPort)

            if (Regex("^@\\w*").find(message) != null) {
                // It is command
                val command = Regex("@\\w*").find(message)?.value
                val argument = message.replace(Regex("^@\\w*\\s*"), "")
                logCommand("$command | $argument\n\tFrom: $userIP:$userPort")
                when (command) {
                    Keyword.HELLO   -> sayHiTo(user)
                    Keyword.NAME    -> trySetNameTo(user, argument)
                    Keyword.QUIT    -> sayByeTo(user)
                    Keyword.CAT     -> cat(user, argument)
                    Keyword.WONAME  -> broadcast(argument, user)
                    else            -> sendTo(user, fPers("Unknown command."))
                }
            } else {
                // Just a message
                broadcast("${fName(user.name)}:\n$message", user)
            }

        }
    }

    private fun cat(receiver: User, argument: String) {
        val trimArg     = argument.trim()
        if (trimArg == "--help") {
            sendTo(receiver, "Format:\n@cat USER \"FILEPATH\"")
            return
        }
        try {
            if (trimArg == "") throw Exception("Not enough arguments.")

            val startQuote  = trimArg.indexOf('\"')
            val endQuote    = trimArg.lastIndexOf('\"')
            if (startQuote == -1 || endQuote == -1 || startQuote == endQuote) {
                throw Exception("Incorrect argument format.")
            }

            val target      = trimArg.substring(0, startQuote).trim()
            if (target == "") throw Exception("No target user specified")

            val targetUser  = connected.find { it.name == target }
            targetUser ?: throw Exception("Target user is not connected")

            val path        = trimArg.substring(startQuote + 1, endQuote)
            sendTo(receiver,
                    "Requested file: $path\nWaiting for $target response...")
            sendTo(targetUser,
                    "${fName(receiver.name)} request access to your file:\n$path\n\n" +
                    "Type \'@yes ${fName(receiver.name)}\' to agree\n" +
                    "or '@no ${fName(receiver.name)}' to reject")
            sendTo(targetUser,
                    "${Keyword.FTINFO} name:\"${receiver.name}\" filepath:\"$path\"")
        } catch (e: Throwable) {
            sendTo(receiver, "Error: ${e.message}\nTry \'@cat --help\' for more information.")
        }
    }

    private fun findOrAddUser(ip: InetAddress, port: Int): User {
        var user: User? = connected.find { it.ip == ip && it.port == port }
        if (user == null) {
            if (logging) logInfo("User connected: $ip:$port")
            user = User(ip = ip, port = port)
            connected = connected.plusElement(user)
            if (logging) logInfo("Connected users: $connected")
        }
        return user
    }

    private fun kick(user: User) {
        connected = connected.minus(user)
        if (logging) logInfo("User disconnected: ${user.ip}:${user.port}")
    }

    private fun sendTo(user: User, message: String) {
        val msg = message.toByteArray()
        socket.send(DatagramPacket(msg, msg.size, user.ip, user.port))
    }

    private fun broadcast(message: String, broadcaster: User) {
        val msg = message.toByteArray()
        connected.filter { it != broadcaster }
                .forEach { socket.send(DatagramPacket(msg, msg.size, it.ip, it.port)) }
    }

    private fun sayHiTo(user: User) {
        val msg = fPers("Welcome to the chat, ${user.name}\n" +
                "Here is a list of available commands:\n" +
                "\t@hello\tdisplay this message\n" +
                "\t@name\tset your name\n" +
                "\t@quit\texit from the chat\n")
        sendTo(user, msg)
        broadcast(fName(user.name) + fLogs(" joined to the chat."), user)
    }

    private fun sayByeTo(user: User) {
        sendTo(user, fPers("Bye!"))
        broadcast(fName(user.name) + fLogs(" left the chat."), user)
        kick(user)
    }

    private fun trySetNameTo(user: User, name: String) {
        when {
            name.trim() == "" -> {
                val failMessage = fPers("Failed to set nickname. Incorrect nickname.")
                sendTo(user, failMessage)
            }
            connected.any { it.name == name } -> {
                val failMessage = fPers("Failed to set nickname ") + fName(name) +
                        fPers(". Nickname already taken.")
                sendTo(user, failMessage)
            }
            else -> {
                val broadcastMsg = fLogs("User ") + fName(user.name) +
                        fLogs(" has changed his nickname to ") + fName(name) + fLogs(".")
                val okMessage = fPers("Success. Now your nickname is ") + fName(name) + fPers(".")
                user.name = name
                sendTo(user, okMessage)
                broadcast(broadcastMsg, user)
            }
        }
    }

    private fun logInfo(message: String) {
        println("\u001b[36mINFO: $message\u001b[0m")
    }

    private fun logCommand(message: String) {
        println("\u001b[31mCOMMAND: $message\u001b[0m")
    }

    private fun fName(string: String): String {
        return "\u001b[34;1m$string\u001b[0m"
    }

    private fun fPers(string: String): String {
        return "\u001b[32;1m$string\u001b[0m"
    }

    private fun fLogs(string: String): String {
        return "\u001b[35m$string\u001b[0m"
    }
}