package com.cornel.lab11chat.client

import java.net.DatagramPacket

class Listener(private val context: Client) : Thread(){
    companion object {
        private val MAX_PACKAGE_SIZE    = 65507
        private val COMMAND_DISCONNECT  = "@bye"
        private val COMMAND_FTINFO      = "@ftinfo"
    }

    override fun run() {
        println("Listening on ${context.socket}")
        loop@ while (true) {
            val receiveData     = ByteArray(MAX_PACKAGE_SIZE)
            val receivePacket   = DatagramPacket(receiveData, receiveData.size)
            context.socket.receive(receivePacket)
            val message = String(receivePacket.data).substring(0, receivePacket.length)
            when {
                message.indexOf(COMMAND_DISCONNECT) == 0    -> break@loop
                message.indexOf(COMMAND_FTINFO) == 0        -> context.addFileRequest(message)
                else                                        -> println(message)
            }
        }
        context.disconnect()
        println("DEBUG: listener is off")
    }
}