package com.cornel.lab11chat

import com.cornel.lab11chat.client.Client
import com.cornel.lab11chat.server.Server

fun main(args: Array<String>) {
    val srv = Server(1337, true)
    srv.start()
    val cli = Client("127.0.0.1", 1337)
    cli.start()
}