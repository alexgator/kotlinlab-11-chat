package com.cornel.lab11chat.client

import com.cornel.lab11chat.common.Keyword
import java.net.DatagramPacket
import java.net.InetAddress
import java.net.DatagramSocket
import java.nio.file.Files
import java.nio.file.Paths

class Client(ip: String, private val port: Int) {
    val socket = DatagramSocket()
    private val listener = Listener(this)
    @Volatile private var fileRequests = listOf<FileRequest>()
    private val serverIP: InetAddress

    init {
        val ipv4mask = Regex("^(\\d{1,3}.){3}\\d{1,3}\$")
        if (!ipv4mask.matches(ip)) throw IllegalArgumentException("Incorrect ip address. Not matching IPv4 format.")

        val ipByte = ByteArray(4)
        ip.split(".").forEachIndexed { index, b ->
            ipByte[index] = b.toByte()
        }
        ipByte.forEach { if (it > 255) throw IllegalArgumentException("Incorrect ip address. Address part is > 255") }

        serverIP = InetAddress.getByAddress(ipByte)

        listener.start()

        send(Keyword.HELLO)
    }

    fun start() {
        while (true) {
            val message = readLine()?.trim()
            if (message == "" || message == null) continue
            
            if (Regex("^@\\w*").find(message) != null) {
                val command = Regex("@\\w*").find(message)?.value
                val argument = message.replace(Regex("^@\\w*\\s*"), "")
                when(command) {
                    "@yes"  -> trySendFile(argument)
                    "@no"   -> rejectSendFile(argument)
                    else    -> send(message)
                }
            } else {
                send(message)
            }
        }
    }

    private fun rejectSendFile(name: String) {
        val request = fileRequests.find { it.name == name }
        if (request == null) {
            println("Incoming request from $name is not found.")
        } else {
            send("Request for file is rejected.")
            fileRequests = fileRequests.minus(request)
        }
    }

    private fun trySendFile(name: String) {
        val request = fileRequests.find { it.name == name }
        if (request == null) {
            println("Incoming request from $name is not found.")
        } else {
            try {
                Files.lines(Paths.get(request.filepath)).forEach {
                    send("@woname $it")
                }
            } catch (e: Exception) {
                send(e.toString())
                println(e.toString())
                println("Abort.")
            }
        }
        fileRequests.minus(request)
    }

    private fun send(message: String) {
        socket.send(DatagramPacket(message.toByteArray(), message.length, serverIP, port))
    }

    @Synchronized fun addFileRequest(info: String) {
        try {
            val userRaw         = Regex("name:\".*?\"").find(info)!!.value
            val user            = userRaw.substring(6, userRaw.length - 1)
            val filepathRaw     = Regex("filepath:\".*?\"").find(info)!!.value
            val filepath        = filepathRaw.substring(10, filepathRaw.length - 1)
            fileRequests        = fileRequests.plus(FileRequest(user, filepath))
        } catch (e: Throwable) {
            println("Error while parsing file transfer request info. Request is not valid.")
        }
    }

    fun disconnect() {
        socket.disconnect()
    }
}