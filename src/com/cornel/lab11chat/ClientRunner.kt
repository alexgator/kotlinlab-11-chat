package com.cornel.lab11chat

import com.cornel.lab11chat.client.Client

fun main(args: Array<String>) {
    val cli = Client("127.0.0.1", 1337)
    cli.start()
}