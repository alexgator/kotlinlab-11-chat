package com.cornel.lab11chat.common

abstract class Keyword {
    companion object {
        val HELLO   = "@hello"
        val NAME    = "@name"
        val CAT     = "@cat"
        val WONAME  = "@woname"
        val FTINFO  = "@ftinfo"
        val QUIT    = "@quit"
    }
}
