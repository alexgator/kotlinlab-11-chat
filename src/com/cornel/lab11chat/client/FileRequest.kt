package com.cornel.lab11chat.client

data class FileRequest(val name: String, val filepath: String)